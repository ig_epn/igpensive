
from obspy.signal import filter
from obspy import Stream, Trace
import numpy as np
import logging
from igpensive.filter import igpFilter
from get_mseed_data import get_mseed
from get_mseed_data import get_mseed_utils as gmutils
import pandas as pd
import db_connection
import json
import os
#np.seterr(divide='ignore', invalid='ignore')



def calculate_rms(stream_data):
    """
    Calculate rms of a time series
    
    :param numpy.ndarray stream_data: data of an obspy stream object 
    
    """
    data_np64=np.array(stream_data,dtype='float64')
    square_data_np64=np.square(data_np64)
    sum_data=square_data_np64.sum()
    media_data=sum_data/data_np64.size
    rms=media_data**(0.5)
    
    return round(rms,4)

def calculate_rsam(stream,filter_list):
    """
    Calculate rsam values according to a filter_list on a mseed stream
    
    """

    rms_data = {'rsam_station':stream[0].stats['station'],'rsam_channel':stream[0].stats['channel'],
                'rsam_datetime':stream[0].stats['starttime']}  
    rms_value = 0
    """
    Check that stream has more than 1 sample ( avoid errors due to obspy.stream.detrend and
    numpy "invalid value encountered in true_divide"
    """
    if len(stream[0].data) > 1 and stream[0].max() !=0:
        stream[0].detrend()
        sampling_rate = stream[0].stats.sampling_rate
        for rsam_filter in filter_list:
            
            ##no aplicar int al valor rsam_filter.order ya que puede ser un valor NONE!!!
            
            corners = (rsam_filter.order, 4)[rsam_filter.order == '' or rsam_filter.order == None or rsam_filter.order == "None"]
            corners = int(corners)
            zerophase = (rsam_filter.zerophase, False)[rsam_filter.zerophase == '' or rsam_filter.zerophase == None or rsam_filter.zerophase == "None"]
            #stream_filtered = 0        
            if rsam_filter.type == "None":
                rms_data[rsam_filter.name] = calculate_rms(stream[0].data)
                rms_value = rms_data[rsam_filter.name] 
            
            elif rsam_filter.type == 'bandpass':
                stream_filtered = filter.bandpass(stream[0].data,rsam_filter.frequency_1,
                                                  rsam_filter.frequency_2,sampling_rate,corners=corners,zerophase=zerophase)
                
                #rms_data[rsam_filter.name] = calculate_rms(stream_filtered[0].data) ##This code generated an error detected by S.Hernandez
                rms_data[rsam_filter.name] = calculate_rms(stream_filtered)
            
            elif rsam_filter.type == 'highpass':
                stream_filtered = filter.highpass(stream[0].data,float(rsam_filter.frequency_1),
                                                  sampling_rate,corners=corners,zerophase=zerophase)
                rms_data[rsam_filter.name] = calculate_rms(stream_filtered)

        for rsam_filter in filter_list:
            
            if rms_data[rsam_filter.name] != "None":
                if rms_data[rsam_filter.name] > rms_value:
                    
                    logging.info("###Error en valor rms: %s vs %s %s" % (rms_value,rsam_filter.name, rms_data[rsam_filter.name] ))
                    rms_data[rsam_filter.name] = 0 
        
    else:
        for rsam_filter in filter_list:
            rms_data[rsam_filter.name] = 0

    
    return rms_data


'''
def create_stream_list(mseed_id,mseed_client,stations_dict,request_starttime,window_length, slice_window,mode):

    
    """
    Retrieve miniSEED data from a miniSEED server for a given time window and station information.
    
    Args:
        mseed_id (str): A string that represents the identifier for the miniSEED data.
        mseed_client (obj): A client that connects to the miniSEED server.
        stations_dict (dict): A dictionary containing station information such as network, code, location, and channel.
        request_starttime (UTCDateTime): A UTCDateTime object representing the start time for data retrieval.
        window_length (int): An integer representing the total length of the window to retrieve data for.
        slice_window (int): An integer representing the length of each slice to retrieve data for.
        mode (str): A string indicating the mode of data retrieval, either "REAL_TIME" or "FILL".
    
    Returns:
        list: A list of ObsPy Stream objects containing the requested data.
    
    Raises:
        None
    


    stream_list = []
    
    for station_key, station in stations_dict['stations'].items():
        for location in station['loc']:
            for channel in station['cha']:
                station_stream = get_mseed.get_stream(mseed_id,mseed_client,station['net'],\
                                                      station['cod'],location,channel,request_starttime +0.01,window_size=window_length)
                                                      #station['cod'],location,channel,request_starttime ,window_size=window_length)
                if station_stream and mode =="REAL_TIME":
                    stream_list.append(station_stream)
                elif station_stream and mode =="FILL":
                    #station_stream.detrend()
                    for x in range(0,int(window_length/slice_window)):
                        slice_starttime = request_starttime + slice_window*x
                        slice_endtime = request_starttime + slice_window*(x+1)
                        sliced_stream = station_stream.slice(slice_starttime,slice_endtime)
                        
                        if len(sliced_stream) != 0:
                            stream_list.append(sliced_stream)
                        else:
                            zero_stream=create_zero_stream(station['net'],station['cod'],location,channel,request_starttime)
                            stream_list.append(zero_stream)                          
                
                elif not station_stream:
                    print("create zero stream")
                    zero_stream=create_zero_stream(station['net'],station['cod'],location,channel,request_starttime)
                    stream_list.append(zero_stream)

    return stream_list
'''

def load_stations_dict():
    stations_file_path = os.path.join('config', 'stations_test.json')
    with open(stations_file_path) as f:
        stations_dict = json.load(f)
    return stations_dict


'''
def create_stream_list(mseed_id, mseed_client, stations_dict, request_starttime, window_length, slice_window, mode):
    print("Contenido de stations_dict en create_stream_list:", json.dumps(stations_dict, indent=2))
    stream_list = []

    try:
        for station_key, station in stations_dict.items():
            for location in station['loc']:
                for channel in station['cha']:
                    station_stream = get_mseed.get_stream(
                        mseed_id,
                        mseed_client,
                        station['net'],
                        station['cod'],
                        location,
                        channel,
                        request_starttime + 0.01,
                        window_size=window_length
                    )
                    if station_stream and mode == "REAL_TIME":
                        stream_list.append(station_stream)
                    elif station_stream and mode == "FILL":
                        for x in range(0, int(window_length / slice_window)):
                            slice_starttime = request_starttime + slice_window * x
                            slice_endtime = request_starttime + slice_window * (x + 1)
                            sliced_stream = station_stream.slice(slice_starttime, slice_endtime)
                            
                            if len(sliced_stream) != 0:
                                stream_list.append(sliced_stream)
                            else:
                                zero_stream = create_zero_stream(station['net'], station['cod'], location, channel, request_starttime)
                                stream_list.append(zero_stream)
                    
                    elif not station_stream:
                        zero_stream = create_zero_stream(station['net'], station['cod'], location, channel, request_starttime)
                        stream_list.append(zero_stream)
    except KeyError as e:
        print(f"Error: Missing key {e} in stations_dict")

    return stream_list
'''

def create_stream_list(mseed_id, mseed_client, stations_dict, day_utc, window_length, overlap, run_mode):
    streams_list = []
    try:
        for station_key, station_info in stations_dict.items():
            for location in station_info['loc']:
                for channel in station_info['cha']:
                    station_stream = get_mseed.get_stream(
                        mseed_id, mseed_client, station_info['net'],
                        station_info['cod'], location, channel, day_utc + 0.01,
                        window_size=window_length
                    )
                    streams_list.append(station_stream)
        return streams_list
    except KeyError as e:
        raise KeyError(f"Falta la clave '{e.args[0]}' en la información de la estación {station_key}")



def create_zero_stream(network,station_code,location,channel,starttime):
    
    trace = Trace()
    trace.stats.starttime = starttime
    trace.stats.network = network
    trace.stats.station = station_code
    trace.stats.location = location
    trace.stats.channel = channel
     
    return Stream(traces=[trace])
    
def create_rsamfilter_list(filters_dict):
    """Create a rsam_filter object from a json file"""
    
    filter_list=[]
    for filter_key,filter in filters_dict.items():       
        igp_filter = igpFilter(**filter)
        filter_list.append(igp_filter)
    
    return filter_list


def insert_data_to_db(t2, t, pressure, azimuth, velocity, mccm, rms, config, name):
    data_frame = pd.data2pandas(t2, t, pressure, azimuth, velocity, mccm, rms, config, name)
    try:
        with open(config.db_config_file, 'r') as f:
            db_config = json.load(f)[config.db_id]
        #influx_client = db_connection.get_influx_client(config.ifx_host, config.ifx_port, config.ifx_user, config.ifx_pass, config.ifx_dbname)
        influx_client = db_connection.get_influx_client(
            db_config['host'],
            db_config['port'],
            db_config['user'],
            db_config['pass'],
            db_config['DBName']
            )
        db_connection.insert_data(data_frame, influx_client)
    except Exception as e:
        print(f"Error in insert_data was: {str(e)}")