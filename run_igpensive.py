
import os
import sys
import logging
import logging.config
#from turtle import st
from obspy import UTCDateTime
from get_mseed_data import get_mseed
from get_mseed_data import get_mseed_utils as gmutils
sys.path.append(os.path.join(os.path.dirname(__file__), 'igpensive'))
#####

from datetime import timedelta, datetime
import pandas as pd
import json
import array_processing
#import config
import utils
#import test_db_connection
import db_connection

import igpensive_utils as igp_utils

"""
import back_populate
"""

# Configuración de logging
if gmutils.check_file("./config/logging.ini"):

    logging.config.fileConfig('./config/logging.ini', disable_existing_loggers=False)
    logger = logging.getLogger('run_rsam')

pd.set_option('display.max_rows',None) 
pd.set_option('display.max_columns',None)
pd.set_option('expand_frame_repr',False)

def main():
    
    is_error=False
    
    if len(sys.argv)==1:
        is_error=True
        print("error al ingresar argumentos")
    else:
        
        try:
           
            run_param = gmutils.read_parameters(sys.argv[1])
            
        except Exception as e:
            logger.error(f"Error reading configuration sets in file: {e}")
            raise Exception("Error reading configuration file: {e}")

        try:

            try:
                processing_params = run_param['PROCESSING']
                
            except KeyError as e:
                logger.error(f"Falta la clave 'PROCESSING': {e}")
                raise Exception(f"Error obteniendo 'PROCESSING': {e}")

            try:
                duration = float(processing_params['duration'])
            except KeyError as e:
                logger.error(f"Falta la clave 'duration' en 'PROCESSING': {e}")
                raise Exception(f"Error obteniendo 'duration': {e}")

            try:
                latency = float(processing_params['latency'])
            except KeyError as e:
                logger.error(f"Falta la clave 'latency' en 'PROCESSING': {e}")
                raise Exception(f"Error obteniendo 'latency': {e}")

            try:
                taper_val = float(processing_params['taper_val'])
            except KeyError as e:
                logger.error(f"Falta la clave 'taper_val' en 'PROCESSING': {e}")
                raise Exception(f"Error obteniendo 'taper_val': {e}")

            try:
                f1 = float(processing_params['f1'])
            except KeyError as e:
                logger.error(f"Falta la clave 'f1' en 'PROCESSING': {e}")
                raise Exception(f"Error obteniendo 'f1': {e}")

            try:
                f2 = float(processing_params['f2'])
            except KeyError as e:
                logger.error(f"Falta la clave 'f2' en 'PROCESSING': {e}")
                raise Exception(f"Error obteniendo 'f2': {e}")

            try:
                window_length = float(processing_params['window_length'])
            except KeyError as e:
                logger.error(f"Falta la clave 'window_length' en 'PROCESSING': {e}")
                raise Exception(f"Error obteniendo 'window_length': {e}")

            try:
                overlap = float(processing_params['overlap'])
            except KeyError as e:
                logger.error(f"Falta la clave 'overlap' en 'PROCESSING': {e}")
                raise Exception(f"Error obteniendo 'overlap': {e}")

            try:
                min_chan = int(processing_params['min_chan'])
            except KeyError as e:
                logger.error(f"Falta la clave 'min_chan' en 'PROCESSING': {e}")
                raise Exception(f"Error obteniendo 'min_chan': {e}")

            try:
                mcthresh = float(processing_params['mcthresh'])
            except KeyError as e:
                logger.error(f"Falta la clave 'mcthresh' en 'PROCESSING': {e}")
                raise Exception(f"Error obteniendo 'mcthresh': {e}")
            
            try:
                working_dir = run_param['paths']['working_dir']
            except KeyError as e:
                logger.error(f"Falta la clave 'working_dir' en 'paths': {e}")
                raise Exception(f"Error obteniendo 'working_dir': {e}")

            try:
                out_valve_dir = run_param['paths']['out_valve_dir']
            except KeyError as e:
                logger.error(f"Falta la clave 'out_valve_dir' en 'paths': {e}")
                raise Exception(f"Error obteniendo 'out_valve_dir': {e}")

            try:
                run_mode = run_param['RUN_MODE']['mode']
            except KeyError as e:
                logger.error(f"Falta la clave 'mode' en 'RUN_MODE': {e}")
                raise Exception(f"Error obteniendo 'run_mode': {e}")

            try:
                data_target = run_param['RUN_MODE']['data_target']
            except KeyError as e:
                logger.error(f"Falta la clave 'data_target' en 'RUN_MODE': {e}")
                raise Exception(f"Error obteniendo 'data_target': {e}")

            try:
                db_id = run_param['DATABASE']['id']
            except KeyError as e:
                logger.error(f"Falta la clave 'id' en 'DATABASE': {e}")
                raise Exception(f"Error obteniendo 'db_id': {e}")

            try:
                db_config_file = run_param['DATABASE']['file_path']
            except KeyError as e:
                logger.error(f"Falta la clave 'file_path' en 'DATABASE': {e}")
                raise Exception(f"Error obteniendo 'db_config_file': {e}")

            try:
                db_type = run_param['DATABASE']['type']
            except KeyError as e:
                logger.error(f"Falta la clave 'type' en 'DATABASE': {e}")
                raise Exception(f"Error obteniendo 'db_type': {e}")

            try:
                db_mode = run_param['DATABASE']['mode']
            except KeyError as e:
                logger.error(f"Falta la clave 'mode' en 'DATABASE': {e}")
                raise Exception(f"Error obteniendo 'db_mode': {e}")

            try:
                mseed_id = run_param['MSEED_SERVER']['id']
            except KeyError as e:
                logger.error(f"Falta la clave 'id' en 'MSEED_SERVER': {e}")
                raise Exception(f"Error obteniendo 'mseed_id': {e}")

            try:
                mseed_server_config_file = run_param['MSEED_SERVER']['file_path']
            except KeyError as e:
                logger.error(f"Falta la clave 'file_path' en 'MSEED_SERVER': {e}")
                raise Exception(f"Error obteniendo 'mseed_server_config_file': {e}")

            try:
                station_file = run_param['STATION_FILE']['file_path']
            except KeyError as e:
                logger.error(f"Falta la clave 'file_path' en 'STATION_FILE': {e}")
                raise Exception(f"Error obteniendo 'station_file': {e}")

            try:
                filter_file = run_param['FILTER_FILE']['file_path']
            except KeyError as e:
                logger.error(f"Falta la clave 'file_path' en 'FILTER_FILE': {e}")
                raise Exception(f"Error obteniendo 'filter_file': {e}")

            try:
                end_time = run_param['TIME']['end_time'] if 'TIME' in run_param and 'end_time' in run_param['TIME'] else None
                if end_time:
                    end_time = end_time.split('#')[0].strip()
                    end_time = UTCDateTime(end_time)
            except KeyError as e:
                logger.error(f"Falta la clave 'end_time' en 'TIME': {e}")
                raise Exception(f"Error obteniendo 'end_time': {e}")

        except Exception as e:
            logger.error(f"Error obteniendo parámetros: {e}")
            logger.error(f"Contenido de run_param: {json.dumps(run_param, indent=2)}")
            raise Exception(f"Error obteniendo parámetros: {e}")
        
        try:
            
            """Read JSON file with parameters"""
            
            # Cargar stations_dict desde stations_test.json usando igpensive_utils
            stations_dict = igp_utils.load_stations_dict() 
            print(f"Loaded stations_dict: {stations_dict}")


            db_param = gmutils.read_config_file(db_config_file)
            mseed_server_param = gmutils.read_config_file(mseed_server_config_file)
            #stations_dict=gmutils.read_config_file(station_file)
                      
            
            
            
            filter_list = gmutils.read_config_file(filter_file)

            #filter_list = gmutils.read_config_file(filter_file)

            # Añadir esta línea para depurar
            # print("Contenido de stations_dict:", json.dumps(stations_dict, indent=2))

        except Exception as e:
            logger.error(f"Failed to read configuration files: {e}")
            raise Exception(f"Failed to read configuration files: {e}")

        if not end_time:
            logger.error("End time parameter is missing")
            raise Exception("End time parameter is missing")
       
        try:
            mseed_client = get_mseed.choose_service(mseed_server_param[mseed_id])
            
            # Crear la lista de filtros
            filter_list = array_processing.create_filter_list(filter_file)
                
            streams_list = array_processing.create_streams_list(
                mseed_id=mseed_id,
                mseed_client=mseed_client,
                stations_dict=stations_dict,
                #stations_dict=stations_dict['stations'],  # Pasar solo las estaciones
                end_time=end_time,
                run_mode=run_mode
            )

            # Procesar los datos utilizando la lista de streams
            igpensive_list = array_processing.process(
                streams_list,
                filter_list,
                end_time,
                out_valve_dir,
                {
                    'duration': duration,
                    'latency': latency,
                    'taper_val': taper_val,
                    'f1': f1,
                    'f2': f2,
                    'window_length': window_length,
                    'overlap': overlap,
                    'min_chan': min_chan,
                    'mcthresh': mcthresh
                },
                #processing_params,
                stations_dict
                #stations_dict['stations']  # Pasar solo las estaciones
            )

       
        except Exception as e:
            logger.error(f"Error in create client mseed_client: {e}")
            raise Exception(f"Error in create client mseed_client: {e}")
            
        # Conectar a la base de datos
        try:
            db_config = db_param[db_id]
            db_client = db_connection.get_influx_client(db_param[db_id]['host'], db_param[db_id]['port'], 
            db_param[db_id]['user'], db_param[db_id]['pass'], db_param[db_id]['dbname'])
        
        except Exception as e:
            logger.error(f"Database connection failed: {e}")
            raise Exception(f"Database connection failed: {e}")

        '''
        # Poblar la base de datos si es necesario
        try:
            back_populate.populate(db_client, igpensive_list)
        except Exception as e:
            logger.error(f"Back population failed: {e}")
            raise Exception(f"Back population failed: {e}")
        '''
        
        #Opciones de salida de datos
        if data_target == "STDOUT":
            print(igpensive_list)
        elif data_target == "CSV":
            utils.save_to_csv(igpensive_list, "igpensive_output.csv")

        elif data_target == "DB" and db_type == "INFLUX":
            logger.info("Insert into INFLUX")
            #influxdb_client = db_connection.get_influx_client(db_param[db_id]['host'], db_param[db_id]['port'], db_param[db_id]['user'],
            db_connection.insert_data(db_client, db_config['DBName'], igpensive_list, filter_list)
            #db_connection.insert_data(influxdb_client, db_param[db_id]['DBName'], igpensive_list, filter_list)
        else:
            logger.error(f"Error in data_target configured: {data_target}")
            raise Exception(f"Error in data_target configured: {data_target}")


    if is_error:
        logger.info(f'Usage: python {sys.argv[0]} configuration_file.txt [start_date] [end_date]')
        print(f'USAGE: python {sys.argv[0]} CONFIGURATION_FILE.txt')


if __name__=='__main__':
    main()


