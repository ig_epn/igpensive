#!/bin/bash

exec 1> >(logger -s -t $(basename $0)) 2>&1
eval "$(/$HOME/anaconda3/bin/conda shell.bash hook)"

conda activate ipensive

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

cd $DIR 
python ./array_processing.py 

