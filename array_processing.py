import os
import sys
import numpy as np
from pandas import DataFrame
from obspy.core import Stream, UTCDateTime
from obspy.core.util import AttribDict
from obspy.clients.earthworm import Client
from obspy.clients.fdsn import Client as Client_IRIS
from obspy.geodetics.base import gps2dist_azimuth
from scipy.signal import correlate
import igpensive_utils as igp_utils
from matplotlib import dates
import time
import utils
import warnings
import traceback
import json
from utils import (
    grab_data, add_coordinate_info, inversion, remove_blank_traces, preprocess_data,
    resample_data, write_valve_file, plot_results, convert_to_utc, print_time_range, get_volcano_backazimuth
)
import utils
import pandas as pd

from igpensive import igpensive_utils as igp_utils
from get_mseed_data import get_mseed_utils as gmutils

import matplotlib as m
m.use('Agg')
from matplotlib import rcParams
import matplotlib.pyplot as plt
from matplotlib import dates
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection
fonts = 10
rcParams.update({'font.size': fonts})


def fetch_data(station_info, t1, t2, latency, window_length):
    scnl_list = [f"{station_info['net']}.{station_info['cod']}.{loc}.{cha}" for loc in station_info['loc'] for cha in station_info['cha']]
    return grab_data(scnl_list, t1 - latency, t2 + latency + window_length, fill_value=0)





#def compute_backazimuth(st, array_info):
#    return utils.get_volcano_backazimuth(st, array_info)
def compute_backazimuth(st, array):
    velocity, azimuth, rms, Cmax, pk_pressure = utils.inversion(st)
    array['velocity'] = velocity
    array['azimuth'] = azimuth
    array['rms'] = rms
    array['Cmax'] = Cmax
    array['pk_pressure'] = pk_pressure
    return array


# Modify the process function in array_processing.py
'''
def process(streams_list, filter_list, end_time, out_valve_dir, processing_params, stations_dict):
    end_time = convert_to_utc(end_time)
    T0 = end_time
    t1 = T0 - processing_params['duration']
    t2 = T0

    print_time_range(t1, t2)

    for station_key, array_info in stations_dict.items():
        print(f'--- {station_key} ---')
        st = fetch_data(array_info, t1, t2, processing_params['latency'], processing_params['window_length'])
        st = add_coordinate_info(st, stations_dict)
        array_info = compute_backazimuth(st, array_info)

        if remove_blank_traces(st, processing_params['min_chan']):
            print('Too many blank traces. Skipping.')
            continue

        preprocess_data(st, t1, t2, processing_params)
        resample_data(st)

        velocity, azimuth, mccm, t, rms, pressure = analyze_windows(st, array_info, t1, t2, processing_params)

        if velocity is not None:
            write_results(st, t2, t, pressure, azimuth, velocity, mccm, rms, out_valve_dir, stations_dict)
'''
def process(streams_list, filter_list, end_time, out_valve_dir, processing_params, stations_dict):
    results = []
    
    for array_name, array_info in stations_dict.items():
        try:
            st = fetch_data(array_info, end_time - processing_params['duration'], end_time, processing_params['latency'], processing_params['window_length'])
            st = add_coordinate_info(st, stations_dict)  # Asegúrate de pasar stations_dict aquí
            array_info = compute_backazimuth(st, array_info)
            results.append(array_info)
        except Exception as e:
            print(f"Error processing array {array_name}: {e}")
    
    return results

def add_metadata(st, station):
    return utils.add_coordinate_info(st, station)


'''
def remove_blank_traces(st, min_chan):
    for tr in st:
        if np.sum(np.abs(tr.data)) == 0:
            st.remove(tr)
    return len(st) < min_chan
'''



def analyze_windows(st, array, t1, t2, processing_params):
    velocity, azimuth, rms, Cmax, pk_pressure = utils.inversion(st)
    t = np.arange(t1.timestamp, t2.timestamp, processing_params['window_length'])
    pressure = [pk_pressure] * len(t)
    return velocity, azimuth, Cmax, t, rms, pressure

def write_results(st, t2, t, pressure, azimuth, velocity, mccm, rms, out_valve_dir, stations_dict, processing_params):
    for array_name, array_info in stations_dict.items():
        write_valve_file(t2, t, pressure, azimuth, velocity, mccm, rms, out_valve_dir, array_name)
        plot_results(t2 - processing_params['duration'], t2, t, st, mccm, velocity, azimuth, array_info, array_info['net'])


def create_filter_list(filter_file):
    filter_dict = gmutils.read_config_file(filter_file)
    filter_list = igp_utils.create_rsamfilter_list(filter_dict)
    return filter_list

def create_streams_list(mseed_id, mseed_client, stations_dict, end_time, run_mode):
    if run_mode == "REAL_TIME":
        day_utc = UTCDateTime(UTCDateTime.now().strftime("%Y-%m-%d %H:%M:00")) - 300
        streams_list = igp_utils.create_stream_list(mseed_id, mseed_client, stations_dict, day_utc, 60, 60, run_mode)
    else:
        streams_list = igp_utils.create_stream_list(mseed_id, mseed_client, stations_dict, end_time, 60, 60, run_mode)
    return streams_list
