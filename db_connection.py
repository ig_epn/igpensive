
#import pypyodbc
#import pyodbc as pypyodbc
#import json
import logging
from influxdb import InfluxDBClient
import pandas as pd

def get_influxdb_client(db_host, db_port,username,password,db_name):
    """
    Create an InfluxDB client.
    
    Args:
        host (str): The hostname or IP address of the InfluxDB server.
        port (int): The port number of the InfluxDB server.
        user (str): The username for authentication.
        password (str): The password for authentication.
        dbname (str): The name of the database.
    
    Returns:
        InfluxDBClient: The InfluxDB client instance.
    """
    
    try:
        client = InfluxDBClient(host=db_host,port=db_port,username=username,password=password, database=db_name)
        return client
    except Exception as e:
        logging.error("Error in get_influxdb_client: %s" %str(e))
        raise Exception("Error in get_influxdb_client: %s" %str(e))
       

def insert_rsam(client_influxdb,influxdb_name,rsam_list,filter_list):
    
    data_list =[]
    #print(val['rsam_station'],val['rsam_channel'],val['rsam_datetime'].strftime("%Y-%m-%dT%H:%M:%S"),val['rms'],val['rsam_banda1'],
    #                  val['rsam_banda2'],val['rsam_banda3'],val['rsam_banda4'],val['rsam_banda5'])
        
    for val in rsam_list:
        for filter in filter_list:
        
            insert_query = "%s,station=%s,channel=%s,banda=%s rsam_value=%s %s" \
                            %(influxdb_name,val['rsam_station'],val['rsam_channel'],
                              filter.name,val[filter.name], int(val['rsam_datetime'].timestamp)) 
    
            data_list.append(insert_query)
    
    #print(data_list)
    #client_influxdb.write("%s,station=%s,channel=%s,banda=%s rsam_value=%s %s" 
    #                      %(influxdb_name,),{'db':influxdb_name})

    client_influxdb.write_points(data_list,time_precision="s",database=influxdb_name,protocol="line")

