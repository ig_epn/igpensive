#out_web_dir='./web/ipensive'
#out_valve_dir='./archivos_csv'
#working_dir='./'

#winston_address='hvo-wws.wr.usgs.gov'
winston_address = '192.168.1.158'
winston_port=16022
network='EC'

influx_db=True
ifx_host='192.168.132.10'
ifx_port=8086
ifx_user='igipensive'
ifx_pass='nehdpl2005'
ifx_dbname='ig_ipensive'

'''
duration      = 600  # DON'T CHANGE THIS!
latency       = 10.0 # seconds between timestamps and end of data window
taper_val     = 5.0  # seconds to taper beginning and end of trace before filtering
#f1		      = 0.5  # minimum frequency for bandpass filter
#f2		      = 10.0 # maximum frequency for bandpass filter
f1            = 0.2
f2            = 4.0
window_length = 30   # window length for each calculation [seconds]
overlap       = 15   # seconds
min_chan      = 3
mcthresh      = 0.5  # where to draw the threshold line in MCCM plot
'''

"""
# Infrasound channels list
arrays=[
	dict({'Name':'Cotopaxi',
		  'SCNL':[
					{'scnl':'RUNA.BDF.EC.01'	, 'sta_lat': -0.594333	, 'sta_lon': -78.467383   },
					{'scnl':'RUNA.BDF.EC.02'	, 'sta_lat': -0.5945    , 'sta_lon': -78.467033  },
					{'scnl':'RUNA.BDF.EC.03'	, 'sta_lat': -0.5943    , 'sta_lon': -78.467017  },
					{'scnl':'RUNA.BDF.EC.04'	, 'sta_lat': -0.594017  , 'sta_lon': -78.467033  },
					{'scnl':'RUNA.BDF.EC.05'	, 'sta_lat': -0.59425   , 'sta_lon': -78.466733 }
					],
		'digouti':  1/(400000*0.0275),
		'volcano':[
					{'name': 'Cotopaxi', 'v_lat': -0.6838,   'v_lon': -78.4372 }]}),

 dict({'Name':'Sangay',
                  'SCNL':[
                                        {'scnl':'SAG1.BDF.EC.01'        , 'sta_lat': -2.193637  , 'sta_lon': -78.099766},
                                        {'scnl':'SAG1.BDF.EC.02'        , 'sta_lat': -2.193408  , 'sta_lon': -78.099465},
                                        {'scnl':'SAG1.BDF.EC.03'       , 'sta_lat': -2.193701  , 'sta_lon': -78.099223},
                                        {'scnl':'SAG1.BDF.EC.04'        , 'sta_lat': -2.193944  , 'sta_lon': -78.099517},
                                        {'scnl':'SAG1.BDF.EC.05'        , 'sta_lat': -2.193641  , 'sta_lon': -78.099506}
                                        ],
                'digouti':  1/(4000000*0.0275),
                'volcano':[
                                        {'name': 'Sangay', 'v_lat': -2.005,   'v_lon': -78.341 }]})

]
"""
