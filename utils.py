'''
import os
import numpy as np
import pandas as pd
from obspy.core import Stream, AttribDict
from obspy.geodetics.base import gps2dist_azimuth
from obspy.clients.earthworm import Client
from obspy.clients.fdsn import Client as Client_IRIS
from scipy.signal import correlate
import matplotlib as m
m.use('Agg')
from matplotlib import rcParams
import matplotlib.pyplot as plt
from matplotlib import dates
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection

fonts = 10
rcParams.update({'font.size': fonts})

def add_coordinate_info(st, stations_dict):
    stations_df = pd.DataFrame.from_dict(stations_dict, orient='index')
    for tr in st:
        if tr.stats.location == '':
            tr.stats.location = '--'
        tmp_scnl = '{}.{}.{}.{}'.format(tr.stats.station, tr.stats.channel, tr.stats.network, tr.stats.location)
        station_info = stations_df[(stations_df.index == tr.stats.station) & 
                                   (stations_df['cha'].apply(lambda x: tr.stats.channel in x)) & 
                                   (stations_df['loc'].apply(lambda x: tr.stats.location in x))]
        if not station_info.empty:
            tmp_lat = station_info['sta_lat'].values[0]
            tmp_lon = station_info['sta_lon'].values[0]
            tr.stats.coordinates = AttribDict({
                'latitude': tmp_lat,
                'longitude': tmp_lon,
                'elevation': 0.0
            })
        else:
            print(f"Warning: No matching station info found for {tmp_scnl}")
    return st

def setup_coordinate_system(st):
    R = 6372.7976   # radius of the earth
    lons = np.array([tr.stats.coordinates.longitude for tr in st])
    lats = np.array([tr.stats.coordinates.latitude for tr in st])
    lon0 = lons.mean() * np.pi / 180.0
    lat0 = lats.mean() * np.pi / 180.0
    yx = R * np.array([lats * np.pi / 180.0 - lat0, (lons * np.pi / 180.0 - lon0) * np.cos(lat0)]).T
    intsd = np.zeros([len(lons), len(lons)])
    ints_az = np.zeros([len(lons), len(lons)])
    for ii in range(len(st[:-1])):
        for jj in range(ii + 1, len(st)):
            tmp = gps2dist_azimuth(lats[ii], lons[ii], lats[jj], lons[jj])
            intsd[ii, jj] = tmp[0]
            ints_az[ii, jj] = tmp[1]
    return yx, intsd, ints_az

def align_stack_stream(st, LAGS):
    st_temp = st.copy()
    group_streams = Stream()
    T1 = st_temp[0].copy().stats.starttime
    T2 = st_temp[0].copy().stats.endtime
    for i, tr in enumerate(st_temp):
        tr = tr.copy().trim(
            tr.stats.starttime - LAGS[i],
            tr.stats.endtime - LAGS[i],
            pad=True,
            fill_value=0,
        )
        tr.trim(tr.stats.starttime + 1, tr.stats.endtime - 1, pad=True, fill_value=0)
        tr.stats.starttime = T1
        group_streams += tr
    ST = group_streams[0].copy()
    for tr in group_streams[1:]:
        ST.data = ST.data + tr.data
    ST.data = ST.data / len(st_temp)
    ST.trim(T1, T2)
    return ST

def inversion(st):
    lags = np.array([])
    Cmax = np.array([])

    mlag = st[0].stats.npts
    tC = np.linspace(-mlag, mlag, 2 * mlag - 1) / st[0].stats.sampling_rate
    for ii in range(len(st[:-1])):
        for jj in range(ii + 1, len(st)):
            scale = np.linalg.norm(st[ii].data) * np.linalg.norm(st[jj].data)
            cc = correlate(st[ii], st[jj], mode='full') / float(scale)
            Cmax = np.append(Cmax, cc.max())
            lags = np.append(lags, tC[cc.argmax()])

    yx, intsd, ints_az = setup_coordinate_system(st)
    ds = intsd[np.triu_indices(len(st), 1)]
    az = ints_az[np.triu_indices(len(st), 1)]

    dt = lags
    Dm3 = np.array([ds * np.cos(az * (np.pi / 180.0)), ds * np.sin(az * (np.pi / 180.0))]).T
    Dm3 = Dm3 / 1000.0  # convert to kilometers

    Gmi = np.linalg.inv(np.matmul(Dm3.T, Dm3))
    sv = np.matmul(np.matmul(Gmi, Dm3.T), dt.T)
    velocity = 1 / np.sqrt(np.square(sv[0]) + np.square(sv[1]))
    caz3 = velocity * sv[0]
    saz3 = velocity * sv[1]
    azimuth = np.arctan2(saz3, caz3) * (180 / np.pi)
    if azimuth < 0:
        azimuth = azimuth + 360
    Dm3_new = np.array([ds * np.cos(az * (np.pi / 180.0)), ds * np.sin(az * (np.pi / 180.0))]).T / 1000
    sv_new = np.array([np.cos(azimuth * np.pi / 180) / velocity, np.sin(azimuth * np.pi / 180) / velocity])
    lags_new = np.matmul(Dm3_new, sv_new)
    rms = np.sqrt(np.mean(np.square(lags_new - dt.T)))
    LAGS = np.hstack((0, lags_new[:len(st) - 1]))
    ST_stack = align_stack_stream(st, LAGS)
    pk_pressure = np.max(np.abs(ST_stack.data))
    return velocity, azimuth, rms, Cmax, pk_pressure

def get_volcano_backazimuth(st, array):
    lon0 = np.mean([tr.stats.coordinates.longitude for tr in st])
    lat0 = np.mean([tr.stats.coordinates.latitude for tr in st])
    for volc in array['volcano']:
        if 'back_azimuth' not in volc:
            tmp = gps2dist_azimuth(lat0, lon0, volc['v_lat'], volc['v_lon'])
            volc['back_azimuth'] = tmp[1]
    return array

def grab_data(scnl, T1, T2, fill_value=0):
    print('Grabbing data...')
    st = Stream()
    client = Client('localhost', 16022)  # Assuming default client settings
    for sta in scnl:
        try:
            tr = client.get_waveforms(sta.split('.')[2], sta.split('.')[0], sta.split('.')[3], sta.split('.')[1], T1, T2)
            if len(tr) > 1:
                if fill_value == 0 or fill_value is None:
                    tr.detrend('demean')
                    tr.taper(max_percentage=0.01)
                for sub_trace in tr:
                    if sub_trace.data.dtype.name != 'int32':
                        sub_trace.data = sub_trace.data.astype('int32')
                    if sub_trace.data.dtype != np.dtype('int32'):
                        sub_trace.data = sub_trace.data.astype('int32')
                    if sub_trace.stats.sampling_rate != np.round(sub_trace.stats.sampling_rate):
                        sub_trace.stats.sampling_rate = np.round(sub_trace.stats.sampling_rate)
                tr.merge(fill_value=fill_value)
            if tr[0].stats.endtime - tr[0].stats.starttime < T2 - T1:
                tr.detrend('demean')
                tr.taper(max_percentage=0.01)
        except:
            tr = Stream()
        if not tr:
            from obspy import Trace
            from numpy import zeros
            tr = Trace()
            tr.stats['station'] = sta.split('.')[0]
            tr.stats['channel'] = sta.split('.')[1]
            tr.stats['network'] = sta.split('.')[2]
            tr.stats['location'] = sta.split('.')[3]
            tr.stats['sampling_rate'] = 100
            tr.stats['starttime'] = T1
            tr.data = zeros(int((T2 - T1) * tr.stats['sampling_rate']), dtype='int32')
        st += tr
    st.trim(T1, T2, pad=True, fill_value=0)
    st.detrend('demean')
    return st

def write_valve_file(t2, t, pressure, azimuth, velocity, mccm, rms, out_valve_dir, name):
    if not os.path.exists(out_valve_dir):
        os.makedirs(out_valve_dir)
    filename = os.path.join(out_valve_dir, f'{name}_{t2.strftime("%Y%m%d-%H%M")}.txt')
    A = pd.DataFrame({'TIMESTAMP': t,
                      'CHANNEL': name,
                      'Azimuth': azimuth,
                      'Velocity': velocity,
                      'MCCM': mccm,
                      'Pressure': pressure,
                      'rms': rms})
    A = A[['TIMESTAMP', 'CHANNEL', 'Azimuth', 'Velocity', 'MCCM', 'Pressure', 'rms']]
    A['Velocity'] = 1000 * A['Velocity']
    A.to_csv(filename, index=False, header=True, sep=',', float_format='%.3f')

def plot_results(t1, t2, t, st, mccm, velocity, azimuth, array, network):
    params_tmp = {var: getattr(config, var) for var in dir(config) if var not in ['NETWORKS'] and '__' not in var}
    for key in array.keys():
        if key in params_tmp.keys():
            params_tmp[key] = array[key]
    tvec = np.linspace(dates.date2num(st[0].stats.starttime.datetime), dates.date2num(st[0].stats.endtime.datetime), len(st[0].data))
    T1 = dates.date2num(t1.datetime)
    T2 = dates.date2num(t2.datetime)
    cm = 'RdYlBu_r'
    cax = 0.2, 1  # colorbar/y-axis for mccm

    fig1 = plt.figure(figsize=(8, 10.5))
    axs1 = plt.subplot(4, 1, 1)
    plt.title(array['Name'] + ' ' + params_tmp['ARRAY_LABEL'] + ' Array')
    axs1.plot(tvec, st[0].data * array['digouti'], 'k', linewidth=0.6)
    axs1.axis('tight')
    axs1.set_xlim(T1, T2)
    ymax = np.abs(list(axs1.get_ylim())).max()
    axs1.set_ylim(-ymax, ymax)
    axs1.xaxis_date()
    axs1.fmt_xdata = dates.DateFormatter('%HH:%MM')
    axs1.xaxis.set_major_formatter(dates.DateFormatter("%H:%M"))
    axs1.set_xticklabels([])
    axs1.tick_params(direction='in', axis='x', top='on')
    axs1.set_ylabel('Pressure [Pa]')
    axs1b = axs1.twinx()
    axs1b.set_yticks([])
    axs1b.set_ylabel('{:.1f} - {:.1f} Hz'.format(params_tmp['FREQMIN'], params_tmp['FREQMAX']), labelpad=6)

    axs2 = plt.subplot(4, 1, 2)
    sc = plt.scatter(t, mccm, c=mccm, edgecolors='k', lw=.3, cmap=cm)
    axs2.plot([T1, T2], [params_tmp['MCTHRESH'], params_tmp['MCTHRESH']], '--', color='gray')
    axs2.axis('tight')
    axs2.set_xlim(T1, T2)
    axs2.set_ylim(cax)
    sc.set_clim(cax)
    axs2.xaxis_date()
    axs2.fmt_xdata = dates.DateFormatter('%HH:%MM')
    axs2.xaxis.set_major_formatter(dates.DateFormatter("%H:%M"))
    axs2.set_xticklabels([])
    axs2.tick_params(direction='in', axis='x', top='on')
    axs2.set_ylabel(r'$M_{d}CCM$')

    axs3 = plt.subplot(4, 1, 3)
    rect = Rectangle((T1, params_tmp['VEL_MIN']), T2 - T1, params_tmp['VEL_MAX'] - params_tmp['VEL_MIN'])
    pc = PatchCollection([rect], facecolor='gray', alpha=0.25, edgecolor=None)
    plt.gca().add_collection(pc)
    sc = axs3.scatter(t, velocity, c=mccm, edgecolors='k', lw=.3, cmap=cm)
    if params_tmp['ARRAY_LABEL'] == 'Hydroacoustic':
        axs3.set_ylim(1.2, 1.8)
    else:
        axs3.set_ylim(.15, .6)
    axs3.set_xlim(T1, T2)
    sc.set_clim(cax)
    axs3.xaxis_date()
    axs3.fmt_xdata = dates.DateFormatter('%HH:%MM')
    axs3.xaxis.set_major_formatter(dates.DateFormatter("%H:%M"))
    axs3.set_xticklabels([])
    axs3.tick_params(direction='in', axis='x', top='on')
    axs3.set_ylabel('Trace Velocity\n [km/s]')

    axs4 = plt.subplot(4, 1, 4)
    if params_tmp['AZ_MAX'] < params_tmp['AZ_MIN']:
        params_tmp['AZ_MIN'] = params_tmp['AZ_MIN'] - 360
        for volc in array['volcano']:
            if volc['back_azimuth'] > 180:
                axs4.plot([T1, T2], [volc['back_azimuth'] - 360, volc['back_azimuth'] - 360], '--', color='gray', zorder=-1)
                axs4.text(t[1], volc['back_azimuth'] - 360, volc['name'], bbox={'facecolor': 'white', 'edgecolor': 'white', 'pad': 0}, fontsize=8, verticalalignment='center', style='italic', zorder=10)
            else:
                axs4.plot([T1, T2], [volc['back_azimuth'], volc['back_azimuth']], '--', color='gray', zorder=-1)
                axs4.text(t[1], volc['back_azimuth'], volc['name'], bbox={'facecolor': 'white', 'edgecolor': 'white', 'pad': 0}, fontsize=8, verticalalignment='center', style='italic', zorder=10)
        azimuth[azimuth > 180] += -360
    else:
        for volc in array['volcano']:
            axs4.plot([T1, T2], [volc['back_azimuth'], volc['back_azimuth']], '--', color='gray', zorder=-1)
            axs4.text(t[1], volc['back_azimuth'], volc['name'], bbox={'facecolor': 'white', 'edgecolor': 'white', 'pad': 0}, fontsize=8, verticalalignment='center', style='italic', zorder=10)
    sc = axs4.scatter(t, azimuth, c=mccm, edgecolors='k', lw=.3, cmap=cm, zorder=1000)
    axs4.set_ylim(params_tmp['AZ_MIN'], params_tmp['AZ_MAX'])
    axs4.set_xlim(T1, T2)
    sc.set_clim(cax)
    axs4.set_ylabel('Back-azimuth\n [deg]')

    axs4.xaxis_date()
    axs4.tick_params(axis='x', labelbottom='on')
    axs4.fmt_xdata = dates.DateFormatter('%HH:%MM')
    axs4.xaxis.set_major_formatter(dates.DateFormatter("%H:%M"))
    axs4.set_xlabel('UTC Time [' + t1.strftime('%Y-%b-%d') + ']')

    plt.subplots_adjust(left=0.1, right=.9, top=0.97, bottom=0.05, hspace=0.1)
    ctop = axs2.get_position().y1
    cbot = axs4.get_position().y0
    cbaxes = fig1.add_axes([.91, cbot, .02, ctop - cbot])
    hc = plt.colorbar(sc, cax=cbaxes)
    hc.set_label(r'$M_{d}CCM$')

    print(array['Name'])
    d0 = os.path.join(out_valve_dir, network, array['Name'], str(t2.year))
    d2 = os.path.join(d0, '{:03d}'.format(t2.julday))
    if not os.path.exists(d2):
        os.makedirs(d2)
    filename = os.path.join(d2, f'{array["Name"]}_{t2.strftime("%Y%m%d-%H%M")}.png')
    plt.savefig(filename, dpi=72, format='png')
    plt.close('all')

    fig1 = plt.figure(figsize=(2.1, 2.75))
    ax_1 = plt.subplot(4, 1, 1)
    ax_1.plot(tvec, st[0].data * array['digouti'], 'k', linewidth=0.1)
    ax_1.axis('tight')
    ymax = np.abs(list(ax_1.get_ylim())).max()
    ax_1.set_ylim(-ymax, ymax)
    ax_1.set_xlim(T1, T2)
    ax_1.set_xticks([])
    ax_1.set_yticks([])

    ax_2 = plt.subplot(4, 1, 2)
    sc = plt.scatter(t, mccm, s=8 * np.ones_like(t), c=mccm, edgecolors='k', lw=.1, cmap=cm)
    ax_2.plot([T1, T2], [params_tmp['MCTHRESH'], params_tmp['MCTHRESH']], '--', color='gray', linewidth=1)
    ax_2.axis('tight')
    ax_2.set_xlim(T1, T2)
    ax_2.set_ylim(cax)
    sc.set_clim(cax)
    ax_2.set_xticks([])
    ax_2.set_yticks([])

    ax_3 = plt.subplot(4, 1, 3)
    rect = Rectangle((T1, params_tmp['VEL_MIN']), T2 - T1, params_tmp['VEL_MAX'] - params_tmp['VEL_MIN'])
    pc = PatchCollection([rect], facecolor='gray', alpha=0.25, edgecolor=None)
    plt.gca().add_collection(pc)
    sc = ax_3.scatter(t, velocity, s=8 * np.ones_like(t), c=mccm, edgecolors='k', lw=.1, cmap=cm)
    if params_tmp['ARRAY_LABEL'] == 'Hydroacoustic':
        ax_3.set_ylim(1.2, 1.8)
    else:
        ax_3.set_ylim(.15, .6)
    ax_3.set_xlim(T1, T2)
    sc.set_clim(cax)
    ax_3.set_xticks([])
    ax_3.set_yticks([])

    ax_4 = plt.subplot(4, 1, 4)
    if params_tmp['AZ_MAX'] < params_tmp['AZ_MIN']:
        params_tmp['AZ_MIN'] = params_tmp['AZ_MIN'] - 360
        azimuth[azimuth > 180] += -360
    sc = ax_4.scatter(t, azimuth, s=8 * np.ones_like(t), c=mccm, edgecolors='k', lw=.3, cmap=cm)
    ax_4.set_ylim(params_tmp['AZ_MIN'], params_tmp['AZ_MAX'])
    ax_4.set_xlim(T1, T2)
    sc.set_clim(cax)
    ax_4.set_xticks([])
    ax_4.set_yticks([])

    plt.subplots_adjust(left=0, right=1, top=0.99, bottom=0.01, hspace=0.03)
    filename = os.path.join(d2, f'{array["Name"]}_{t2.strftime("%Y%m%d-%H%M")}_thumb.png')
    plt.savefig(filename, format='png', pad_inches=0, dpi=72)
    plt.close('all')

def compute_backazimuth(st, array):
    velocity, azimuth, rms, Cmax, pk_pressure = inversion(st)
    array['velocity'] = velocity
    array['azimuth'] = azimuth
    array['rms'] = rms
    array['Cmax'] = Cmax
    array['pk_pressure'] = pk_pressure
    return array

def analyze_windows(st, array, t1, t2, processing_params):
    velocity, azimuth, rms, Cmax, pk_pressure = inversion(st)
    t = np.arange(t1.timestamp, t2.timestamp, processing_params['window_length'])
    pressure = [pk_pressure] * len(t)
    return velocity, azimuth, Cmax, t, rms, pressure

def write_results(st, t2, t, pressure, azimuth, velocity, mccm, rms, out_valve_dir, stations_dict):
    for array_name, array_info in stations_dict.items():
        write_valve_file(t2, t, pressure, azimuth, velocity, mccm, rms, out_valve_dir, array_name)
        plot_results(t2 - processing_params['duration'], t2, t, st, mccm, velocity, azimuth, array_info, array_info['net'])

def convert_to_utc(time_str):
    return UTCDateTime(time_str)

def print_time_range(t1, t2):
    print(f"{t1} - {t2}")
'''

import os
import numpy as np
import pandas as pd
from obspy.core import Stream, UTCDateTime, Trace, AttribDict
#from obspy.core.util import AttribDict
from obspy.clients.earthworm import Client
from obspy.clients.fdsn import Client as Client_IRIS
from obspy.geodetics.base import gps2dist_azimuth
from scipy.signal import correlate
import matplotlib.pyplot as plt
from matplotlib import dates
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection

import pandas as pd
from obspy.core import AttribDict

import pandas as pd

import json
#from obspy.core import AttribDict

######
def load_configuration_file(file_path):
    config = {}
    with open(file_path, 'r') as f:
        section = None
        for line in f:
            line = line.strip()
            if not line or line.startswith("#"):
                continue
            if line.startswith("[") and line.endswith("]"):
                section = line[1:-1]
                config[section] = {}
            elif "=" in line and section:
                name, value = line.split('=', 1)
                name = name.strip()
                value = value.strip()
                config[section][name] = value
    return config

def load_stations(file_path):
    with open(file_path, 'r') as f:
        stations = json.load(f)
    return stations


def load_multiple_stations(file_paths):
    combined_stations = {}
    for file_path in file_paths:
        with open(file_path, 'r') as f:
            stations = json.load(f)
            combined_stations.update(stations)
    return combined_stations

#######


def add_coordinate_info(st, stations_dict):
    # Convertir el diccionario de estaciones en un DataFrame sin usar los nombres de las estaciones como índice
    stations_df = pd.DataFrame.from_dict(stations_dict).T.reset_index(drop=True)
    print(f"Stations DataFrame:\n{stations_df}")
    
    for tr in st:
        # Depuración de los Metadatos Originales
        print(f"Original Trace Stats: Network={tr.stats.network}, Station={tr.stats.station}, Location={tr.stats.location}, Channel={tr.stats.channel}")
        
        if tr.stats.location == '':
            tr.stats.location = '--'
        
        # Asegurar que los metadatos estén limpios
        tr.stats.network = tr.stats.network.strip()
        tr.stats.station = tr.stats.station.strip()
        tr.stats.location = tr.stats.location.strip()
        tr.stats.channel = tr.stats.channel.strip()
        
        # Depuración de los Metadatos Limpiados
        print(f"Cleaned Trace Stats: Network={tr.stats.station}, Station={tr.stats.channel}, Location={tr.stats.network}, Channel={tr.stats.location}")
        
        tmp_scnl = f"{tr.stats.station}.{tr.stats.channel}.{tr.stats.network}.{tr.stats.location}"
        
        # Lógica de filtrado ajustada
        station_info = stations_df[
            (stations_df['net'] == tr.stats.station) & 
            (stations_df['cod'] == tr.stats.channel) & 
            (stations_df['loc'].apply(lambda x: tr.stats.network in x)) & 
            (stations_df['cha'].apply(lambda x: tr.stats.location in x))
        ]
        
        # Depuración de los Resultados del Filtrado
        print(f"Filtering for: net={tr.stats.network}, cod={tr.stats.station}, loc contains {tr.stats.location}, cha contains {tr.stats.channel}")
        print("Filtered Station Info:\n", station_info)
        print(f"Looking for SCNL: {tmp_scnl}, found: {station_info}")
        
        if not station_info.empty:
            tmp_lat = station_info['sta_lat'].values[0]
            tmp_lon = station_info['sta_lon'].values[0]
            tr.stats.coordinates = AttribDict({
                'latitude': tmp_lat,
                'longitude': tmp_lon,
                'elevation': 0.0
            })
        else:
            print(f"Warning: No matching station info found for {tmp_scnl}")
    return st

def setup_coordinate_system(st):
    R = 6372.7976   # radius of the earth
    lons = np.array([tr.stats.coordinates.longitude for tr in st if hasattr(tr.stats, 'coordinates') and tr.stats.coordinates.longitude is not None])
    lats = np.array([tr.stats.coordinates.latitude for tr in st if hasattr(tr.stats, 'coordinates') and tr.stats.coordinates.latitude is not None])
    if len(lons) == 0 or len(lats) == 0:
        raise ValueError("No valid coordinates found in the stream")
    lon0 = lons.mean() * np.pi / 180.0
    lat0 = lats.mean() * np.pi / 180.0
    yx = R * np.array([lats * np.pi / 180.0 - lat0, (lons * np.pi / 180.0 - lon0) * np.cos(lat0)]).T
    intsd = np.zeros([len(lons), len(lons)])
    ints_az = np.zeros([len(lons), len(lons)])
    for ii in range(len(lons[:-1])):
        for jj in range(ii + 1, len(lons)):
            tmp = gps2dist_azimuth(lats[ii], lons[ii], lats[jj], lons[jj])
            intsd[ii, jj] = tmp[0]
            ints_az[ii, jj] = tmp[1]
    return yx, intsd, ints_az

def align_stack_stream(st, LAGS):
    st_temp = st.copy()
    group_streams = Stream()
    T1 = st_temp[0].copy().stats.starttime
    T2 = st_temp[0].copy().stats.endtime
    for i, tr in enumerate(st_temp):
        tr = tr.copy().trim(
            tr.stats.starttime - LAGS[i],
            tr.stats.endtime - LAGS[i],
            pad=True,
            fill_value=0,
        )
        tr.trim(tr.stats.starttime + 1, tr.stats.endtime - 1, pad=True, fill_value=0)
        tr.stats.starttime = T1
        group_streams += tr
    ST = group_streams[0].copy()
    for tr in group_streams[1:]:
        ST.data = ST.data + tr.data
    ST.data = ST.data / len(st_temp)
    ST.trim(T1, T2)
    return ST

def inversion(st):
    lags = np.array([])
    Cmax = np.array([])

    mlag = st[0].stats.npts
    tC = np.linspace(-mlag, mlag, 2 * mlag - 1) / st[0].stats.sampling_rate
    for ii in range(len(st[:-1])):
        for jj in range(ii + 1, len(st)):
            scale = np.linalg.norm(st[ii].data) * np.linalg.norm(st[jj].data)
            cc = correlate(st[ii], st[jj], mode='full') / float(scale)
            Cmax = np.append(Cmax, cc.max())
            lags = np.append(lags, tC[cc.argmax()])

    yx, intsd, ints_az = setup_coordinate_system(st)
    ds = intsd[np.triu_indices(len(st), 1)]
    az = ints_az[np.triu_indices(len(st), 1)]

    dt = lags
    Dm3 = np.array([ds * np.cos(az * (np.pi / 180.0)), ds * np.sin(az * (np.pi / 180.0))]).T
    Dm3 = Dm3 / 1000.0  # convert to kilometers

    Gmi = np.linalg.inv(np.matmul(Dm3.T, Dm3))
    sv = np.matmul(np.matmul(Gmi, Dm3.T), dt.T)
    velocity = 1 / np.sqrt(np.square(sv[0]) + np.square(sv[1]))
    caz3 = velocity * sv[0]
    saz3 = velocity * sv[1]
    azimuth = np.arctan2(saz3, caz3) * (180 / np.pi)
    if azimuth < 0:
        azimuth = azimuth + 360
    Dm3_new = np.array([ds * np.cos(az * (np.pi / 180.0)), ds * np.sin(az * (np.pi / 180.0))]).T / 1000
    sv_new = np.array([np.cos(azimuth * np.pi / 180) / velocity, np.sin(azimuth * np.pi / 180) / velocity])
    lags_new = np.matmul(Dm3_new, sv_new)
    rms = np.sqrt(np.mean(np.square(lags_new - dt.T)))
    LAGS = np.hstack((0, lags_new[:len(st) - 1]))
    ST_stack = align_stack_stream(st, LAGS)
    pk_pressure = np.max(np.abs(ST_stack.data))
    return velocity, azimuth, rms, Cmax, pk_pressure

def get_volcano_backazimuth(st, array):
    lon0 = np.mean([tr.stats.coordinates.longitude for tr in st])
    lat0 = np.mean([tr.stats.coordinates.latitude for tr in st])
    for volc in array['volcano']:
        if 'back_azimuth' not in volc:
            tmp = gps2dist_azimuth(lat0, lon0, volc['v_lat'], volc['v_lon'])
            volc['back_azimuth'] = tmp[1]
    return array

def grab_data(scnl, T1, T2, fill_value=0):
    print('Grabbing data...')
    st = Stream()
    client = Client('localhost', 16022)  # Assuming default client settings
    for sta in scnl:
        try:
            tr = client.get_waveforms(sta.split('.')[2], sta.split('.')[0], sta.split('.')[3], sta.split('.')[1], T1, T2)
            if len(tr) > 1:
                if fill_value == 0 or fill_value is None:
                    tr.detrend('demean')
                    tr.taper(max_percentage=0.01)
                for sub_trace in tr:
                    if sub_trace.data.dtype.name != 'int32':
                        sub_trace.data = sub_trace.data.astype('int32')
                    if sub_trace.data.dtype != np.dtype('int32'):
                        sub_trace.data = sub_trace.data.astype('int32')
                    if sub_trace.stats.sampling_rate != np.round(sub_trace.stats.sampling_rate):
                        sub_trace.stats.sampling_rate = np.round(sub_trace.stats.sampling_rate)
                tr.merge(fill_value=fill_value)
            if tr[0].stats.endtime - tr[0].stats.starttime < T2 - T1:
                tr.detrend('demean')
                tr.taper(max_percentage=0.01)
        except:
            tr = Stream()
        if not tr:
            tr = Trace()
            tr.stats['station'] = sta.split('.')[0]
            tr.stats['channel'] = sta.split('.')[1]
            tr.stats['network'] = sta.split('.')[2]
            tr.stats['location'] = sta.split('.')[3]
            tr.stats['sampling_rate'] = 100
            tr.stats['starttime'] = T1
            tr.data = np.zeros(int((T2 - T1) * tr.stats['sampling_rate']), dtype='int32')
        st += tr
    st.trim(T1, T2, pad=True, fill_value=0)
    st.detrend('demean')
    return st

'''

def compute_backazimuth(st, array):
    velocity, azimuth, rms, Cmax, pk_pressure = inversion(st)
    array['velocity'] = velocity
    array['azimuth'] = azimuth
    array['rms'] = rms
    array['Cmax'] = Cmax
    array['pk_pressure'] = pk_pressure
    return array

def analyze_windows(st, array, t1, t2, processing_params):
    velocity, azimuth, rms, Cmax, pk_pressure = inversion(st)
    t = np.arange(t1.timestamp, t2.timestamp, processing_params['window_length'])
    pressure = [pk_pressure] * len(t)
    return velocity, azimuth, Cmax, t, rms, pressure

def write_results(st, t2, t, pressure, azimuth, velocity, mccm, rms, out_valve_dir, stations_dict, processing_params):
    for array_name, array_info in stations_dict.items():
        write_valve_file(t2, t, pressure, azimuth, velocity, mccm, rms, out_valve_dir, array_name)
        plot_results(t2 - processing_params['duration'], t2, t, st, mccm, velocity, azimuth, array_info, array_info['net'])


def convert_to_utc(time_str):
    return UTCDateTime(time_str)

def print_time_range(t1, t2):
    print(f"{t1} - {t2}")
'''
def write_valve_file(t2, t, pressure, azimuth, velocity, mccm, rms, out_valve_dir, name):
    A = pd.DataFrame({'TIMESTAMP': t,
                      'CHANNEL': name,
                      'Azimuth': azimuth,
                      'Velocity': velocity,
                      'MCCM': mccm,
                      'Pressure': pressure,
                      'rms': rms})

    A = A[['TIMESTAMP', 'CHANNEL', 'Azimuth', 'Velocity', 'MCCM', 'Pressure', 'rms']]
    A['Velocity'] = 1000 * A['Velocity']
    filename = os.path.join(out_valve_dir, f'{name}_{t2.strftime("%Y%m%d-%H%M")}.txt')
    A.to_csv(filename, index=False, header=True, sep=',', float_format='%.3f')

'''
def plot_results(t1, t2, t, st, mccm, velocity, azimuth, array, network, processing_params):
    params_tmp = {'MCTHRESH': 0.8, 'VEL_MIN': 0.2, 'VEL_MAX': 1.0, 'AZ_MIN': 0, 'AZ_MAX': 360, 'ARRAY_LABEL': 'Infrasound'}
    params_tmp['duration'] = t2 - t1
    params_tmp['f1'] = 0.5
    params_tmp['f2'] = 10.0

    tvec = np.linspace(dates.date2num(st[0].stats.starttime.datetime), dates.date2num(st[0].stats.endtime.datetime), len(st[0].data))
    T1 = dates.date2num(t1.datetime)
    T2 = dates.date2num(t2.datetime)
    cm = 'RdYlBu_r'
    cax = 0.2, 1

    fig1 = plt.figure(figsize=(8, 10.5))
    axs1 = plt.subplot(4, 1, 1)
    plt.title(array['Name'] + ' ' + params_tmp['ARRAY_LABEL'] + ' Array')
    axs1.plot(tvec, st[0].data * array['digouti'], 'k', linewidth=0.6)
    axs1.axis('tight')
    axs1.set_xlim(T1, T2)
    ymax = np.abs(list(axs1.get_ylim())).max()
    axs1.set_ylim(-ymax, ymax)
    axs1.xaxis_date()
    axs1.fmt_xdata = dates.DateFormatter('%HH:%MM')
    axs1.xaxis.set_major_formatter(dates.DateFormatter("%H:%M"))
    axs1.set_xticklabels([])
    axs1.tick_params(direction='in', axis='x', top='on')
    axs1.set_ylabel('Pressure [Pa]')
    axs1b = axs1.twinx()
    axs1b.set_yticks([])
    axs1b.set_ylabel('{:.1f} - {:.1f} Hz'.format(params_tmp['f1'], params_tmp['f2']), labelpad=6)

    axs2 = plt.subplot(4, 1, 2)
    sc = plt.scatter(t, mccm, c=mccm, edgecolors='k', lw=.3, cmap=cm)
    axs2.plot([T1, T2], [params_tmp['MCTHRESH'], params_tmp['MCTHRESH']], '--', color='gray')
    axs2.axis('tight')
    axs2.set_xlim(T1, T2)
    axs2.set_ylim(cax)
    sc.set_clim(cax)
    axs2.xaxis_date()
    axs2.fmt_xdata = dates.DateFormatter('%HH:%MM')
    axs2.xaxis.set_major_formatter(dates.DateFormatter("%H:%M"))
    axs2.set_xticklabels([])
    axs2.tick_params(direction='in', axis='x', top='on')
    axs2.set_ylabel(r'$M_{d}CCM$')

    axs3 = plt.subplot(4, 1, 3)
    rect = Rectangle((T1, params_tmp['VEL_MIN']), T2 - T1, params_tmp['VEL_MAX'] - params_tmp['VEL_MIN'])
    pc = PatchCollection([rect], facecolor='gray', alpha=0.25, edgecolor=None)
    plt.gca().add_collection(pc)
    sc = axs3.scatter(t, velocity, c=mccm, edgecolors='k', lw=.3, cmap=cm)
    axs3.set_xlim(T1, T2)
    sc.set_clim(cax)
    axs3.xaxis_date()
    axs3.fmt_xdata = dates.DateFormatter('%HH:%MM')
    axs3.xaxis.set_major_formatter(dates.DateFormatter("%H:%M"))
    axs3.set_xticklabels([])
    axs3.tick_params(direction='in', axis='x', top='on')
    axs3.set_ylabel('Trace Velocity\n [km/s]')

    axs4 = plt.subplot(4, 1, 4)
    for volc in array['volcano']:
        axs4.plot([T1, T2], [volc['back_azimuth'], volc['back_azimuth']], '--', color='gray', zorder=-1)
        axs4.text(t[1], volc['back_azimuth'], volc['name'], bbox={'facecolor': 'white', 'edgecolor': 'white', 'pad': 0},
                  fontsize=8, verticalalignment='center', style='italic', zorder=10)
    sc = axs4.scatter(t, azimuth, c=mccm, edgecolors='k', lw=.3, cmap=cm, zorder=1000)
    axs4.set_ylim(params_tmp['AZ_MIN'], params_tmp['AZ_MAX'])
    axs4.set_xlim(T1, T2)
    sc.set_clim(cax)
    axs4.set_ylabel('Back-azimuth\n [deg]')

    axs4.xaxis_date()
    axs4.tick_params(axis='x', labelbottom='on')
    axs4.fmt_xdata = dates.DateFormatter('%HH:%MM')
    axs4.xaxis.set_major_formatter(dates.DateFormatter("%H:%M"))
    axs4.set_xlabel('UTC Time [' + t1.strftime('%Y-%b-%d') + ']')

    plt.subplots_adjust(left=0.1, right=.9, top=0.97, bottom=0.05, hspace=0.1)
    ctop = axs2.get_position().y1
    cbot = axs4.get_position().y0
    cbaxes = fig1.add_axes([.91, cbot, .02, ctop - cbot])
    hc = plt.colorbar(sc, cax=cbaxes)
    hc.set_label(r'$M_{d}CCM$')

    d0 = os.path.join(network, array['Name'], str(t2.year))
    d2 = os.path.join(d0, '{:03d}'.format(t2.julday))
    filename = os.path.join(d2, f'{array["Name"]}_{t2.strftime("%Y%m%d-%H%M")}.png')
    plt.savefig(filename, dpi=72, format='png')
    plt.close('all')

    fig1 = plt.figure(figsize=(2.1, 2.75))
    ax_1 = plt.subplot(4, 1, 1)
    ax_1.plot(tvec, st[0].data * array['digouti'], 'k', linewidth=0.1)
    ax_1.axis('tight')
    ymax = np.abs(list(ax_1.get_ylim())).max()
    ax_1.set_ylim(-ymax, ymax)
    ax_1.set_xlim(T1, T2)
    ax_1.set_xticks([])
    ax_1.set_yticks([])

    ax_2 = plt.subplot(4, 1, 2)
    sc = plt.scatter(t, mccm, s=8 * np.ones_like(t), c=mccm, edgecolors='k', lw=.1, cmap=cm)
    ax_2.plot([T1, T2], [params_tmp['MCTHRESH'], params_tmp['MCTHRESH']], '--', color='gray', linewidth=1)
    ax_2.axis('tight')
    ax_2.set_xlim(T1, T2)
    ax_2.set_ylim(cax)
    sc.set_clim(cax)
    ax_2.set_xticks([])
    ax_2.set_yticks([])

    ax_3 = plt.subplot(4, 1, 3)
    rect = Rectangle((T1, params_tmp['VEL_MIN']), T2 - T1, params_tmp['VEL_MAX'] - params_tmp['VEL_MIN'])
    pc = PatchCollection([rect], facecolor='gray', alpha=0.25, edgecolor=None)
    plt.gca().add_collection(pc)
    sc = ax_3.scatter(t, velocity, s=8 * np.ones_like(t), c=mccm, edgecolors='k', lw=.1, cmap=cm)
    ax_3.set_xlim(T1, T2)
    sc.set_clim(cax)
    ax_3.set_xticks([])
    ax_3.set_yticks([])

    ax_4 = plt.subplot(4, 1, 4)
    sc = ax_4.scatter(t, azimuth, s=8 * np.ones_like(t), c=mccm, edgecolors='k', lw=.3, cmap=cm)
    ax_4.set_ylim(params_tmp['AZ_MIN'], params_tmp['AZ_MAX'])
    ax_4.set_xlim(T1, T2)
    sc.set_clim(cax)
    ax_4.set_xticks([])
    ax_4.set_yticks([])

    plt.subplots_adjust(left=0, right=1, top=0.99, bottom=0.01, hspace=0.03)
    filename = os.path.join(d2, f'{array["Name"]}_{t2.strftime("%Y%m%d-%H%M")}_thumb.png')
    plt.savefig(filename, format='png', pad_inches=0, dpi=72)
    plt.close('all')
'''
def remove_blank_traces(st, min_chan):
    for tr in st:
        if np.sum(np.abs(tr.data)) == 0:
            st.remove(tr)
    return len(st) < min_chan

def preprocess_data(st, t1, t2, processing_params):
    st.detrend('demean')
    st.taper(max_percentage=None, max_length=processing_params['taper_val'])
    st.filter('bandpass', freqmin=processing_params['f1'], freqmax=processing_params['f2'], corners=2, zerophase=True)
    st.trim(t1, t2 + processing_params['window_length'] + 1)

def resample_data(st):
    for tr in st:
        if tr.stats['sampling_rate'] == 100:
            tr.decimate(2)
        if tr.stats['sampling_rate'] != 50:
            tr.resample(50.0)
        if tr.stats['sampling_rate'] == 50:
            tr.decimate(2)

def convert_to_utc(time):
    return UTCDateTime(time)

def print_time_range(t1, t2):
    print(f'{t1} - {t2}')

def plot_results(t1, t2, t, st, mccm, velocity, azimuth, array, network, processing_params, out_valve_dir):
    # get default params from config
    params_tmp = processing_params

    ########## big plot ##########
    ##############################
    tvec = np.linspace(dates.date2num(st[0].stats.starttime.datetime), dates.date2num(st[0].stats.endtime.datetime), len(st[0].data))
    T1 = dates.date2num(t1.datetime)
    T2 = dates.date2num(t2.datetime)
    cm = 'RdYlBu_r'
    cax = 0.2, 1   # colorbar/y-axis for mccm

    fig1 = plt.figure(figsize=(8, 10.5))
    axs1 = plt.subplot(4, 1, 1)
    plt.title(array['Name'] + ' ' + params_tmp['ARRAY_LABEL'] + ' Array')
    axs1.plot(tvec, st[0].data * array['digouti'], 'k', linewidth=0.6)
    axs1.axis('tight')
    axs1.set_xlim(T1, T2)
    ymax = np.abs(list(axs1.get_ylim())).max()
    axs1.set_ylim(-ymax, ymax)
    axs1.xaxis_date()
    axs1.fmt_xdata = dates.DateFormatter('%HH:%MM')
    axs1.xaxis.set_major_formatter(dates.DateFormatter("%H:%M"))
    axs1.set_xticklabels([])
    axs1.tick_params(direction='in', axis='x', top='on')
    axs1.set_ylabel('Pressure [Pa]')
    axs1b = axs1.twinx()
    axs1b.set_yticks([])
    axs1b.set_ylabel('{:.1f} - {:.1f} Hz'.format(params_tmp['FREQMIN'], params_tmp['FREQMAX']), labelpad=6)

    axs2 = plt.subplot(4, 1, 2)
    sc = plt.scatter(t, mccm, c=mccm, edgecolors='k', lw=.3, cmap=cm)
    axs2.plot([T1, T2], [params_tmp['MCTHRESH'], params_tmp['MCTHRESH']], '--', color='gray')
    axs2.axis('tight')
    axs2.set_xlim(T1, T2)
    axs2.set_ylim(cax)
    sc.set_clim(cax)
    axs2.xaxis_date()
    axs2.fmt_xdata = dates.DateFormatter('%HH:%MM')
    axs2.xaxis.set_major_formatter(dates.DateFormatter("%H:%M"))
    axs2.set_xticklabels([])
    axs2.tick_params(direction='in', axis='x', top='on')
    axs2.set_ylabel(r'$M_{d}CCM$')

    axs3 = plt.subplot(4, 1, 3)
    rect = Rectangle((T1, params_tmp['VEL_MIN']), T2 - T1, params_tmp['VEL_MAX'] - params_tmp['VEL_MIN'])
    pc = PatchCollection([rect], facecolor='gray', alpha=0.25, edgecolor=None)
    plt.gca().add_collection(pc)
    sc = axs3.scatter(t, velocity, c=mccm, edgecolors='k', lw=.3, cmap=cm)
    if params_tmp['ARRAY_LABEL'] == 'Hydroacoustic':
        axs3.set_ylim(1.2, 1.8)
    else:
        axs3.set_ylim(.15, .6)
    axs3.set_xlim(T1, T2)
    sc.set_clim(cax)
    axs3.xaxis_date()
    axs3.fmt_xdata = dates.DateFormatter('%HH:%MM')
    axs3.xaxis.set_major_formatter(dates.DateFormatter("%H:%M"))
    axs3.set_xticklabels([])
    axs3.tick_params(direction='in', axis='x', top='on')
    axs3.set_ylabel('Trace Velocity\n [km/s]')

    axs4 = plt.subplot(4, 1, 4)

    if params_tmp['AZ_MAX'] < params_tmp['AZ_MIN']:
        params_tmp['AZ_MIN'] = params_tmp['AZ_MIN'] - 360
        for volc in array['volcano']:
            if volc['back_azimuth'] > 180:
                axs4.plot([T1, T2], [volc['back_azimuth'] - 360, volc['back_azimuth'] - 360], '--', color='gray', zorder=-1)
                axs4.text(t[1], volc['back_azimuth'] - 360, volc['name'], bbox={'facecolor': 'white', 'edgecolor': 'white', 'pad': 0}, fontsize=8, verticalalignment='center', style='italic', zorder=10)
            else:
                axs4.plot([T1, T2], [volc['back_azimuth'], volc['back_azimuth']], '--', color='gray', zorder=-1)
                axs4.text(t[1], volc['back_azimuth'], volc['name'], bbox={'facecolor': 'white', 'edgecolor': 'white', 'pad': 0}, fontsize=8, verticalalignment='center', style='italic', zorder=10)
        azimuth[azimuth > 180] += -360
    else:
        for volc in array['volcano']:
            axs4.plot([T1, T2], [volc['back_azimuth'], volc['back_azimuth']], '--', color='gray', zorder=-1)
            axs4.text(t[1], volc['back_azimuth'], volc['name'], bbox={'facecolor': 'white', 'edgecolor': 'white', 'pad': 0}, fontsize=8, verticalalignment='center', style='italic', zorder=10)
    sc = axs4.scatter(t, azimuth, c=mccm, edgecolors='k', lw=.3, cmap=cm, zorder=1000)
    axs4.set_ylim(params_tmp['AZ_MIN'], params_tmp['AZ_MAX'])
    axs4.set_xlim(T1, T2)
    sc.set_clim(cax)
    axs4.set_ylabel('Back-azimuth\n [deg]')

    axs4.xaxis_date()
    axs4.tick_params(axis='x', labelbottom='on')
    axs4.fmt_xdata = dates.DateFormatter('%HH:%MM')
    axs4.xaxis.set_major_formatter(dates.DateFormatter("%H:%M"))
    axs4.set_xlabel('UTC Time [' + t1.strftime('%Y-%b-%d') + ']')

    plt.subplots_adjust(left=0.1, right=.9, top=0.97, bottom=0.05, hspace=0.1)
    ctop = axs2.get_position().y1
    cbot = axs4.get_position().y0
    cbaxes = fig1.add_axes([.91, cbot, .02, ctop - cbot])
    hc = plt.colorbar(sc, cax=cbaxes)
    hc.set_label(r'$M_{d}CCM$')

    print(array['Name'])
    d0 = os.path.join(out_valve_dir, network, array['Name'], str(t2.year))
    d2 = os.path.join(d0, '{:03d}'.format(t2.julday))
    filename = os.path.join(d2, array['Name'] + '_' + t2.strftime('%Y%m%d-%H%M') + '.png')
    plt.savefig(filename, dpi=72, format='png')
    plt.close('all')

    ######### small plot #########
    ##############################
    fig1 = plt.figure(figsize=(2.1, 2.75))
    ax_1 = plt.subplot(4, 1, 1)
    ax_1.plot(tvec, st[0].data * array['digouti'], 'k', linewidth=0.1)
    ax_1.axis('tight')
    ymax = np.abs(list(ax_1.get_ylim())).max()
    ax_1.set_ylim(-ymax, ymax)
    ax_1.set_xlim(T1, T2)
    ax_1.set_xticks([])
    ax_1.set_yticks([])

    ax_2 = plt.subplot(4, 1, 2)
    sc = plt.scatter(t, mccm, s=8 * np.ones_like(t), c=mccm, edgecolors='k', lw=.1, cmap=cm)
    ax_2.plot([T1, T2], [params_tmp['MCTHRESH'], params_tmp['MCTHRESH']], '--', color='gray', linewidth=1)
    ax_2.axis('tight')
    ax_2.set_xlim(T1, T2)
    ax_2.set_ylim(cax)
    sc.set_clim(cax)
    ax_2.set_xticks([])
    ax_2.set_yticks([])

    ax_3 = plt.subplot(4, 1, 3)
    rect = Rectangle((T1, params_tmp['VEL_MIN']), T2 - T1, params_tmp['VEL_MAX'] - params_tmp['VEL_MIN'])
    pc = PatchCollection([rect], facecolor='gray', alpha=0.25, edgecolor=None)
    plt.gca().add_collection(pc)
    sc = ax_3.scatter(t, velocity, s=8 * np.ones_like(t), c=mccm, edgecolors='k', lw=.1, cmap=cm)
    if params_tmp['ARRAY_LABEL'] == 'Hydroacoustic':
        ax_3.set_ylim(1.2, 1.8)
    else:
        ax_3.set_ylim(.15, .6)
    ax_3.set_xlim(T1, T2)
    sc.set_clim(cax)
    ax_3.set_xticks([])
    ax_3.set_yticks([])

    ax_4 = plt.subplot(4, 1, 4)
    if params_tmp['AZ_MAX'] < params_tmp['AZ_MIN']:
        params_tmp['AZ_MIN'] = params_tmp['AZ_MIN'] - 360
        azimuth[azimuth > 180] += -360

    sc = ax_4.scatter(t, azimuth, s=8 * np.ones_like(t), c=mccm, edgecolors='k', lw=.3, cmap=cm)
    ax_4.set_ylim(params_tmp['AZ_MIN'], params_tmp['AZ_MAX'])
    ax_4.set_xlim(T1, T2)
    sc.set_clim(cax)
    ax_4.set_xticks([])
    ax_4.set_yticks([])

    plt.subplots_adjust(left=0, right=1, top=0.99, bottom=0.01, hspace=0.03)
    filename = os.path.join(d2, array['Name'] + '_' + t2.strftime('%Y%m%d-%H%M') + '_thumb.png')
    plt.savefig(filename, format='png', pad_inches=0, dpi=72)
    plt.close('all')
