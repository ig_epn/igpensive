'''
Created on Dec 9, 2021

@author: wacero
'''
import sys
import db_connection
import config
import pandas as pd
"""
ifx_host='192.168.1.90'
ifx_port=8086
ifx_user='sangay'
ifx_pass='sangay2020'
ifx_dbname='cotopaxi_ipensive'
"""
ifx_host='192.168.132.10'
ifx_port=8086
ifx_user='igipensive'
ifx_pass='nehdpl2005'
ifx_dbname='ig_ipensive'


data = {
    'timestamp': ['2023-04-04T20:40:15', '2023-04-04T20:40:30', '2023-04-04T20:40:45', '2023-04-04T20:41:00', '2023-04-04T20:41:15'],
    'station': ['SAG1', 'RUNA', 'SAG1', 'SAG1', 'RUNA'],
    'azimuth': [104.476198, 63.992046, 278.818705, 358.726019, 214.411670],
    'velocity': [4.388803, 23.711728, 19.873610, 9.644971, 28.080184],
    'MCCM': [0.163470, 0.162886, 0.172908, 0.221834, 0.179162],
    'pressure': [0.024053, 0.024053, 0.010211, 0.008568, 0.008735],
    'RMS': [3.114223, 4.354313, 2.201728, 5.532046, 2.032457]
}

df = pd.DataFrame(data)
print(df["station"][0])
sys.exit()

df['timestamp'] = pd.to_datetime(df['timestamp'],format='%Y-%m-%d %H:%M:%S')
df.set_index('timestamp', inplace=True)
print(df.head(2))
measurement='infrasound'
tag_columns = ['station']


print("START")

def test_influx_client():
    
    #influx_client = db_connection.get_influx_client(config.ifx_host,config.ifx_port,config.ifx_user,config.ifx_pass,config.ifx_dbname)
    influx_client = db_connection.get_influx_client(ifx_host,ifx_port,ifx_user, ifx_pass, ifx_dbname)

    print(type(influx_client))
    
    #response = influx_client.write_points(df, measurement, tag_columns=tag_columns, field_columns=['azimuth', 'velocity', 'MCCM', 'pressure', 'RMS'], time_precision='ms', database=ifx_dbname, protocol='json', batch_size=10000)
    response = influx_client.write_points(df,"ipensive",protocol="line")    

    print(response)
test_influx_client()
